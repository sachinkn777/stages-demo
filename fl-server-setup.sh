#!/bin/bash
sudo apt update -y
sudo apt install docker.io -y
sudo apt install unzip -y
sudo docker pull nvcr.io/nvidia/clara-train-sdk:v4.0 &
echo "0.0.0.0 flserver" >> /etc/hosts
#Pull the server.zip from the GCS Bucket and unzip
gsutil -m cp -r gs://datacopy-bucket/provisioning/server.zip .
unzip -oP 0zOeNT3shZVjMmIK server.zip 
cd startup/
sudo su
chmod 700 docker.sh start.sh && bash docker.sh
docker exec -it flserver /bin/bash
hostname

